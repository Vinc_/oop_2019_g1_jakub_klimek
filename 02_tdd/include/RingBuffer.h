#pragma once

class RingBuffer {
public:

    explicit RingBuffer(unsigned int providedCapacity = 10u);
    ~RingBuffer();

    bool empty() const;
    unsigned int size() const;
    unsigned int capacity() const;

    void push(int item);
    int pop();

private:
    unsigned int providedCapacity;

    int* data;
    unsigned int head{0};
    unsigned int tail{0};
};