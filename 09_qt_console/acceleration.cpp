#include "acceleration.h"

Acceleration::Acceleration(QObject *parent) : QObject(parent)
{

}

float Acceleration::get()
{
    return second - first;
}

void Acceleration::onChange(float v)
{
    first = second;
    second = v;
}
