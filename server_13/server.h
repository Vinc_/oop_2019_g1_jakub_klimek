#ifndef SERVER_H
#define SERVER_H

#include <QObject>
#include <connection.h>

class Server : public QObject
{
    Q_OBJECT
public:
    explicit Server(QObject *parent = nullptr);
    ~Server();
    void run();

private:
    QTcpServer* tcpServer = nullptr;
    Connection* connection = nullptr;


signals:

public slots:
    void createConnection();

};

#endif // SERVER_H
