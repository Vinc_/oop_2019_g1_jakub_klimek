#include "RingBuffer.h"

RingBuffer::RingBuffer(unsigned int providedCapacity)
    : providedCapacity(providedCapacity) {

    data = new int[providedCapacity];
}

RingBuffer::~RingBuffer() {

    delete [] data;
}

bool RingBuffer::empty() const {

    return head == tail;
}

unsigned int RingBuffer::size() const {

    return head - tail;
}

unsigned int RingBuffer::capacity() const {

    return providedCapacity;
}

void RingBuffer::push(int item) {

    auto index = head % providedCapacity;

    if (size() == capacity())
        ++tail;

    ++head;

    data[index] = item;
}

int RingBuffer::pop() {

    auto index = tail % providedCapacity;
    ++tail;

    return data[index];
}
