# Object Oriented Programming II

Group: 1

- 01\_cmake - DONE
- 02\_tdd - DONE
- 03\_sequence\_containers - DONE
- 04\_associative\_containers - DONE
- 05\_stl\_benchmark - DONE
- 06\_observer\_pattern - DONE
- 07\_abstract\_factory\_pattern - DONE
- 08\_fake\_mock\_or\_stub - DONE
- 09\_qt\_\* - DONE
- 10\_state\_machine - DONE
- 11\_qml - CURRENT

This project contains **googletest** and **benchamrk** submodules added using following command:

```bash
git submodule add https://github.com/google/googletest.git
git submodule add https://github.com/google/benchmark.git
```

To properly clone this repository remember to add **--recursive** to clone command:

```bash
git clone [url] --recursive
```

Or exeucte this command after cloning:

```bash
git submodule update --init
```

