#include "speed.h"

Speed::Speed(QObject *parent) : QObject(parent)
{

}

float Speed::get()
{
    return second - first;
}

void Speed::onChange(float v)
{
    first = second;
    second = v;

    emit changed(get());
}

