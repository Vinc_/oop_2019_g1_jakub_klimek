#include <QCoreApplication>

#include "displacement.h"
#include "speed.h"
#include "acceleration.h"

#include <QDebug>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    Displacement displacement{};
    Speed speed{};
    Acceleration acceleration{};

    QObject::connect(&displacement, SIGNAL(changed(float)),
                     &speed, SLOT(onChange(float)));

    QObject::connect(&speed, SIGNAL(changed(float)),
                     &acceleration, SLOT(onChange(float)));


    qDebug() << "Run...";

    displacement.set(1);
    displacement.set(2);

    qDebug() << "Speed: " << speed.get();

    displacement.set(3);

    qDebug() << "Speed: " << speed.get();
    qDebug() << "Acceleration: " << acceleration.get();

    displacement.set(5);

    qDebug() << "Speed: " << speed.get();
    qDebug() << "Acceleration: " << acceleration.get();

    qDebug() << "Done.";

    return a.exec();
}
