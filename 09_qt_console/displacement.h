#ifndef DISPLACEMENT_H
#define DISPLACEMENT_H

#include <QObject>

class Displacement : public QObject
{
    Q_OBJECT
public:
    explicit Displacement(QObject *parent = nullptr);

    void set(float v);

signals:
    void changed(float v);

public slots:
};

#endif // DISPLACEMENT_H
