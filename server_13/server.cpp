#include "server.h"
#include <QDebug>

Server::Server(QObject *parent) : QObject(parent)
{

}


void Server::run()
{
    tcpServer = new QTcpServer(this);
    const QHostAddress address = QHostAddress("127.0.0.1");
    quint16 port = 8000;
        if (!tcpServer->listen(address, port)) {
            qDebug() << "serv error";
            return;
        }
   connect(tcpServer, SIGNAL(newConnection()), this, SLOT(createConnection()));
}


void Server::createConnection()
{
    connection = new Connection(tcpServer->nextPendingConnection());
}

Server::~Server()
{
    delete tcpServer;
    //tcpServer->close();
}
