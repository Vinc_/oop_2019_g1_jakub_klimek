#ifndef SPEED_H
#define SPEED_H

#include <QObject>

class Speed : public QObject
{
    Q_OBJECT
public:
    explicit Speed(QObject *parent = nullptr);

    float get();

signals:
    void changed(float v);

public slots:
    void onChange(float v);

private:
    float first{};
    float second{};
};

#endif // SPEED_H
