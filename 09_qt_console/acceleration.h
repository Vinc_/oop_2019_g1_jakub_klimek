#ifndef ACCELERATION_H
#define ACCELERATION_H

#include <QObject>

class Acceleration : public QObject
{
    Q_OBJECT
public:
    explicit Acceleration(QObject *parent = nullptr);

    float get();

signals:

public slots:
    void onChange(float v);

private:
    float first{};
    float second{};
};

#endif // ACCELERATION_H
