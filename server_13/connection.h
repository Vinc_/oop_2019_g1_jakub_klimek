#ifndef CONNECTION_H
#define CONNECTION_H

#include <QObject>
#include <QTcpServer>

class Connection : public QObject
{
    Q_OBJECT
public:
    explicit Connection(QTcpSocket* _socket, QObject *parent = nullptr);

    QTcpSocket* socket = nullptr;

signals:

public slots:
    void onReadyRead();
    void onDisconnect();
};

#endif // CONNECTION_H
