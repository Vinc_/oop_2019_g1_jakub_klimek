#include "TestIncludes.h"
#include "RingBuffer.h"

TEST(RingBuffer, Constructor) {

    RingBuffer ringBuffer{};
}

TEST(RingBuffer, Empty_TrueWhenCreated) {

    RingBuffer ringBuffer{};
    EXPECT_TRUE(ringBuffer.empty());
}

TEST(RingBuffer, Size_ZeroWhenCreated) {

    RingBuffer ringBuffer{};
    EXPECT_EQ(0u, ringBuffer.size());
}

TEST(RingBuffer, Capacity_Default) {

    RingBuffer ringBuffer{};
    EXPECT_EQ(10u, ringBuffer.capacity());
}

TEST(RingBuffer, Capacity_CustomValue) {

    RingBuffer ringBuffer{3u};
    EXPECT_EQ(3u, ringBuffer.capacity());
}

TEST(RingBuffer, Push_Pop_CapacityOne) {

    RingBuffer ringBuffer{1u};
    ringBuffer.push(3);
    EXPECT_EQ(3, ringBuffer.pop());
}

TEST(RingBuffer, Push_Pop_CapacityOne_OtherValue) {

    RingBuffer ringBuffer{1u};
    ringBuffer.push(4);
    EXPECT_EQ(4, ringBuffer.pop());
}

TEST(RingBuffer, Push_TwoTimes_Pop_ReturnSecondValue) {

    RingBuffer ringBuffer{1u};
    ringBuffer.push(6);
    ringBuffer.push(7);
    EXPECT_EQ(7, ringBuffer.pop());
}

TEST(RingBuffer, Push_NotEmpty_Pop_Empty) {

    RingBuffer ringBuffer{1u};
    ringBuffer.push(3);
    EXPECT_FALSE(ringBuffer.empty());
    ringBuffer.pop();
    EXPECT_TRUE(ringBuffer.empty());
}

TEST(RingBuffer, Push_SizeOne_Pop_SizeZero) {

    RingBuffer ringBuffer{1u};
    ringBuffer.push(3);
    EXPECT_EQ(1u, ringBuffer.size());
    ringBuffer.pop();
    EXPECT_EQ(0u, ringBuffer.size());
}

TEST(RingBuffer, CapacityTwo_PushTwoTimes_PopTwoTimes) {

    RingBuffer ringBuffer{2u};
    ringBuffer.push(3);
    ringBuffer.push(4);
    EXPECT_EQ(3, ringBuffer.pop());
    EXPECT_EQ(4, ringBuffer.pop());
}

TEST(RingBuffer, CapacityTwo_PushMoreThanTwoTimes_PopTwoTimes_CheckSize) {

    RingBuffer ringBuffer{2u};
    ringBuffer.push(3);
    EXPECT_EQ(1u, ringBuffer.size());
    ringBuffer.push(4);
    EXPECT_EQ(2u, ringBuffer.size());
    ringBuffer.push(5);
    EXPECT_EQ(2u, ringBuffer.size());
    ringBuffer.push(6);
    EXPECT_EQ(2u, ringBuffer.size());
    EXPECT_EQ(5, ringBuffer.pop());
    EXPECT_EQ(1u, ringBuffer.size());
    EXPECT_EQ(6, ringBuffer.pop());
    EXPECT_EQ(0u, ringBuffer.size());
}
