#include "connection.h"
#include <QTextStream>
#include <QTcpSocket>
#include <QString>

Connection::Connection(QTcpSocket* _socket, QObject *parent) : QObject(parent)
{
    socket = _socket;
    connect(socket, SIGNAL(readyRead()), this, SLOT(onReadyRead()));
    connect(socket, SIGNAL(close()), this, SLOT(onDisconnect()));
}


void Connection::onReadyRead()
{
    QString str;
    QTextStream out(socket);
    str = out.readLine();
    QTextStream in(socket);
    in << str.toUpper() + '\n';
}


void Connection::onDisconnect()
{
    socket->close();
}
